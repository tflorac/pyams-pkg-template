===================
{{ cookiecutter.project_name }} package
===================

Introduction
------------

This package is composed of a set of utility functions, usable into any Pyramid application.

    >>> from pyramid.testing import setUp, tearDown
    >>> config = setUp(hook_zca=True)

    >>> from {{ cookiecutter.project_slug }} import *

    >>> tearDown()
