#
# Copyright (c) 2015-2019 Thierry Florac <tflorac AT ulthar.net>
# All Rights Reserved.
#
# This software is subject to the provisions of the Zope Public License,
# Version 2.1 (ZPL).  A copy of the ZPL should accompany this distribution.
# THIS SOFTWARE IS PROVIDED "AS IS" AND ANY AND ALL EXPRESS OR IMPLIED
# WARRANTIES ARE DISCLAIMED, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF TITLE, MERCHANTABILITY, AGAINST INFRINGEMENT, AND FITNESS
# FOR A PARTICULAR PURPOSE.
#

"""{{ cookiecutter.project_name }}.include module

This module is used for Pyramid integration
"""

import os.path

import {{ cookiecutter.project_slug }}


__docformat__ = 'restructuredtext'


def include_package(config):
    """Pyramid package include"""

    # add translations
    config.add_translation_dirs('{{ cookiecutter.project_slug }}:locales')

    try:
        import pyams_zmi  # pylint: disable=import-outside-toplevel,unused-import
    except ImportError:
        config.scan(ignore='{{ cookiecutter.project_slug }}.zmi')
    else:
        config.scan()

    if hasattr(config, 'load_zcml'):
        zcml_name = os.path.join({{ cookiecutter.project_slug }}.__path__[0], 'configure.zcml')
        if os.path.exists(zcml_name):
            config.load_zcml(zcml_name)
